function getItemByVitamin(rawData,vitamin){
    return rawData.filter(item => item.contains === vitamin);
}

module.exports = getItemByVitamin;