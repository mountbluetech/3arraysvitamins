function itemsContainVitamin(rawData, vitamin){
    return rawData.filter(item => item.contains.includes(vitamin));
}

module.exports = itemsContainVitamin;