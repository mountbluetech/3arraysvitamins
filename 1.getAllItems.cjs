function getAllItems(rawData){
    return rawData.filter(item => item.available);
}

module.exports = getAllItems;