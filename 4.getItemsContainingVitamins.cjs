function getItemsContainingVitamins(rawData){
    return rawData.reduce((accu, item) =>{
        item.contains.split(', ').forEach(i => {
            if(accu[i]){
                accu[i].push(item.name);
            }
            else{
                accu[i] = [item.name];
            }
        })
        return accu;
    },{});

}
module.exports = getItemsContainingVitamins;