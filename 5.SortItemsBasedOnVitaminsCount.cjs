
function sortItemsBAsedOnVitaminsCount(rawData){
    // return Object.fromEntries(Object.entries(Data).sort((item1, item2) => item1[1].length - item2[1].length));
      return rawData.sort((item1, item2) => item1.contains.split(", ").length - item2.contains.split(", ").length);
}

module.exports = sortItemsBAsedOnVitaminsCount;