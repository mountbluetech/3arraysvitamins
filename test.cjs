let rawData = require('./data/3-arrays-vitamins.cjs')
let getAllItems = require('./1.getAllItems.cjs');
let getItemByVitamin = require('./2.getItemByVitamin.cjs');
let itemsContainVitamin = require('./3.itemsContainVitamin.cjs');
let getItemsContainingVitamins = require('./4.getItemsContainingVitamins.cjs');
let sortByVitaminsCount = require('./5.SortItemsBasedOnVitaminsCount.cjs')

let allItems_1 = getAllItems(rawData);
console.log(allItems_1);

let byVitamin = getItemByVitamin(rawData,"Vitamin C")
console.log(byVitamin);

let itemscontainingVitamin = itemsContainVitamin(rawData, "Vitamin A")
console.log(itemscontainingVitamin);

let itemsContainingVitamins = getItemsContainingVitamins(rawData);
console.log(itemsContainingVitamins)

// let sortedbyVitaminsCount = sortByVitaminsCount(itemsContainingVitamins);
// console.log(sortedbyVitaminsCount);

let sortedbyVitaminsCount = sortByVitaminsCount(rawData);
console.log(sortedbyVitaminsCount);


module.exports = itemsContainingVitamins;